package com.example.intro.se.exeptions;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@ApiModel
public class ApiError {

    @ApiModelProperty(notes = "Status of the HTTP response", example = "404")
    private int status;

    @ApiModelProperty(notes = "Message of the HTTP response depends on the status", example = "Not Found")
    private String statusMessage;

    @ApiModelProperty(notes = "Timestamp when this error occurred")
    private String timestamp;

    @ApiModelProperty(notes = "Detailed errors")
    private List<ApiErrorDetail> errors;
}