package com.example.intro.se.utils;

import com.example.intro.se.domain.User;
import com.example.intro.se.dto.request.Base.UserBase;
import com.example.intro.se.dto.request.UserCreateRequestDTO;
import com.example.intro.se.dto.request.UserUpdateRequestDTO;
import com.example.intro.se.dto.response.UserResponseDTO;

public class UserUtils {
    public static UserResponseDTO toDTO(User user){
        return UserResponseDTO.builder()
                .name(user.getName())
                .gender(user.getGender())
                .birthday(user.getBirthday())
                .address(user.getAddress())
                .telNo(user.getTelNo())
                .build();
    }
    public static User toDAO(User user, UserUpdateRequestDTO updateRequestDTO){
        user.setAddress(updateRequestDTO.getAddress());
        user.setTelNo(updateRequestDTO.getTelNo());
        return user;
    }
    public static User toDAO(UserCreateRequestDTO createRequestDTO){
        return User.builder()
                .id(createRequestDTO.getId())
                .name(createRequestDTO.getName())
                .gender(createRequestDTO.getGender())
                .birthday(createRequestDTO.getBirthday())
                .address(createRequestDTO.getAddress())
                .telNo(createRequestDTO.getTelNo())
                .build();
    }
    public static User toDAO(Long id, UserBase userBase){
        return User.builder()
                .id(id)
                .name(userBase.getName())
                .gender(userBase.getGender())
                .birthday(userBase.getBirthday())
                .address(userBase.getAddress())
                .telNo(userBase.getTelNo())
                .build();
    }
}
