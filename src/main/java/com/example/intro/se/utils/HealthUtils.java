package com.example.intro.se.utils;

import com.example.intro.se.domain.HealthReport;
import com.example.intro.se.dto.request.HealthReportCreateRequestDTO;
import com.example.intro.se.dto.request.InsurranceUpdateRequestDTO;
import com.example.intro.se.dto.response.HealthResponseDTO;
import com.example.intro.se.dto.response.InsurranceUpdateResponseDTO;

public class HealthUtils {
    public static HealthResponseDTO toDTO(HealthReport healthReport){
        return HealthResponseDTO.builder()
                .id(healthReport.getId())
                .userId(healthReport.getUserId())
                .insurranceId(healthReport.getInsurranceId())
                .fever(healthReport.isFever())
                .cough(healthReport.isCough())
                .shortWinded(healthReport.isShortWinded())
                .pneumonia(healthReport.isPneumonia())
                .soreThroat(healthReport.isSoreThroat())
                .tired(healthReport.isTired())
                .chronicLiver(healthReport.isChronicLiver())
                .chronicBlood(healthReport.isChronicLiver())
                .chronicLung(healthReport.isChronicLung())
                .chronicKidney(healthReport.isChronicKidney())
                .heartRelatedDiseases(healthReport.isHeartRelatedDiseases())
                .build();
    }
    public static HealthReport toDAO(Long userId, HealthReportCreateRequestDTO createRequestDTO){
        return HealthReport.builder()
                .userId(userId)
                .insurranceId(createRequestDTO.getInsurranceId())
                .fever(createRequestDTO.isFever())
                .cough(createRequestDTO.isCough())
                .shortWinded(createRequestDTO.isShortWinded())
                .pneumonia(createRequestDTO.isPneumonia())
                .soreThroat(createRequestDTO.isSoreThroat())
                .tired(createRequestDTO.isTired())
                .chronicLiver(createRequestDTO.isChronicLiver())
                .chronicBlood(createRequestDTO.isChronicBlood())
                .chronicLung(createRequestDTO.isChronicLung())
                .chronicKidney(createRequestDTO.isChronicKidney())
                .heartRelatedDiseases(createRequestDTO.isHeartRelatedDiseases())
                .build();
    }
    public static HealthReport toDAO(HealthReport healthReport, InsurranceUpdateRequestDTO updateRequestDTO){
        healthReport.setUserId(updateRequestDTO.getUserId());
        healthReport.setInsurranceId(updateRequestDTO.getInsurranceId());
        return healthReport;
    }

    public static InsurranceUpdateResponseDTO toInsurranceDTO(HealthReport healthReport){
        return InsurranceUpdateResponseDTO.builder()
                .id(healthReport.getId())
                .userId(healthReport.getUserId())
                .newInsurranceId(healthReport.getInsurranceId())
                .build();
    }
}
