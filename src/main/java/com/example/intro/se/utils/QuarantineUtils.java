package com.example.intro.se.utils;

import com.example.intro.se.domain.QuarantineReport;
import com.example.intro.se.dto.request.QuarantineReportCreateRequestDTO;
import com.example.intro.se.dto.response.QuarantineResponseDTO;

public class QuarantineUtils {
    public static QuarantineResponseDTO toDTO(QuarantineReport entity){
        return QuarantineResponseDTO.builder()
                .id(entity.getId())
                .userId(entity.getUserId())
                .type(entity.getType())
                .addr(entity.getAddr())
                .time(entity.getTime())
                .room(entity.getRoom())
                .bed(entity.getBed())
                .build();
    }
    public static QuarantineReport toDAO(Long userId,QuarantineReportCreateRequestDTO createRequestDTO){
        return QuarantineReport.builder()
                .userId(userId)
                .type(createRequestDTO.getType())
                .addr(createRequestDTO.getAddr())
                .time(createRequestDTO.getTime())
                .room(createRequestDTO.getRoom())
                .bed(createRequestDTO.getBed())
                .build();
    }
}
