package com.example.intro.se.utils.sendmail;

import javax.mail.MessagingException;
import java.util.LinkedList;
import java.util.Queue;

public class MailSenderThread extends Thread {
    private Thread t;
    private String threadName;
    private Queue<String> emailQueue = new LinkedList<>();
    private Integer mailSent = 0;
    private SendGmail emailSender = new SendGmail();
    public MailSenderThread(String name) {
        threadName = name;
        System.out.println("Creating " + threadName);
    }

    public void send(String receiver) {
        emailQueue.add(receiver);
        // if (!this.isAlive()) this.start();
    }

    @Override
    public void run() {
        System.out.println("Running " + threadName);
        while(emailQueue.peek() != null){
            String receiver = emailQueue.poll(); 
            try {
            emailSender.sendHTML(receiver);
            mailSent++;
            System.out.println(receiver);
            } catch (MessagingException e) {
            System.out.println("Error  sending mail to" + receiver);
            }
        }
        System.out.println("Thread " + threadName + " exiting.");
    }
 
    public void start() {
        System.out.println("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
 
}