package com.example.intro.se.utils.sendmail;

import java.util.LinkedList;
import java.util.Queue;

public class EmailThreadController {
    public static MailSenderThread t = new MailSenderThread("Thread-2-Send-Email");
    private static Queue<String> emailQueue = new LinkedList<>();
    public static void send(String receiver){
        emailQueue.add(receiver);
        if (!t.isAlive()){
            t = new MailSenderThread("Thread-2-Send-Email");
            t.send(receiver);
            t.start();
        }
        else t.send(receiver);
    }
}
