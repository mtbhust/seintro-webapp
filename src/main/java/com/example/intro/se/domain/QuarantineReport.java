package com.example.intro.se.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "quarantine_report")
public class QuarantineReport {
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name ="user_id", nullable = false)
    private Long userId;

    @Column(name = "quarantine_type", nullable = false, length = 45)
    private String type;

    @Column(name = "quarantine_address", nullable = false, length = 45)
    private String addr;

    @Column(name = "quarantine_time", nullable = false)
    private Date time;

    @Column(name = "quarantine_room", length = 45)
    private String room;

    @Column(name = "quarantine_bed", length = 45)
    private String bed;
}
