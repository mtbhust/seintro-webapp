package com.example.intro.se.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Table(name = "covid_test")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CovidTest {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name= "quarantine_id", nullable = false)
    private Long quarantineId;

    @Column(name = "test_times", nullable = false)
    private int times;

    @Column(name = "test_kind", nullable = false, length = 45)
    private String kind;

    @Column(name = "test_result", nullable = false, length = 45)
    private String result;

    @Column(name = "test_time", nullable = false)
    private Date time;
}
