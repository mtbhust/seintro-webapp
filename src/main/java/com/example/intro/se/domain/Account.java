package com.example.intro.se.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "account")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "user_name", length = 45, nullable = false)
    private String userName;

    @Column(name = "user_password", length = 45, nullable = false)
    private String password;
}
