package com.example.intro.se.repository;

import com.example.intro.se.domain.CovidTest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CovidTestRepository extends JpaRepository<CovidTest, Long> {
    List<CovidTest> findAllByQuarantineId(Long quarantineId);
}
