package com.example.intro.se.repository;

import com.example.intro.se.domain.EpidReport;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EpidReportRepository extends JpaRepository<EpidReport, Long> {
    List<EpidReport> findAllByUserId(Long id);
}
