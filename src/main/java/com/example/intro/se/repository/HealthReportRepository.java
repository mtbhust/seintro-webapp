package com.example.intro.se.repository;

import com.example.intro.se.domain.HealthReport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HealthReportRepository extends JpaRepository<HealthReport, Long> {

}
