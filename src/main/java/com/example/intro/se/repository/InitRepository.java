package com.example.intro.se.repository;

import com.example.intro.se.domain.Init;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InitRepository extends JpaRepository<Init, Long>{

}
