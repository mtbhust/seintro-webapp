package com.example.intro.se.dto.request;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class LoginRequestDTO {
    private String userName;
    private String password;

}
