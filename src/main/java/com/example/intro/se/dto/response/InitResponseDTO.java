package com.example.intro.se.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Setter
@Getter
@Builder
public class InitResponseDTO {
    private String name;
    private String tellNo;
    private Date createdOn;
}
