package com.example.intro.se.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Builder
public class UserResponseDTO {
    private String name;

    private String gender;

    @JsonFormat(pattern="dd/MM/yyyy")
    private Date birthday;

    private String address;

    private String telNo;
}
