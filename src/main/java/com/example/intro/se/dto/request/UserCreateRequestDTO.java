package com.example.intro.se.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserCreateRequestDTO {
    private Long id;

    private String name;

    private String gender;

    @ApiModelProperty(example = "dd/MM/yyyy")
    @JsonFormat(pattern="dd/MM/yyyy")
    private Date birthday;

    private String address;

    private String telNo;
}
