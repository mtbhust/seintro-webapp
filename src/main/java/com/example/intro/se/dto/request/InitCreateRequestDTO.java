package com.example.intro.se.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class InitCreateRequestDTO {
    private String name;
    private String telNo;
}
