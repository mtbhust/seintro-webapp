package com.example.intro.se.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class CovidTestCreateRequestDTO {
    private String kind;

    private String result;

    @ApiModelProperty(example = "dd/MM/yyyy")
    @JsonFormat(pattern="dd/MM/yyyy")
    private Date time;
}
