package com.example.intro.se.controller;

import com.example.intro.se.domain.EpidReport;
import com.example.intro.se.dto.request.EpidCreateRequestDTO;
import com.example.intro.se.dto.request.Generic.CreateReportRequestDTO;
import com.example.intro.se.service.EpidService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/epidemiology")
@AllArgsConstructor
public class EpidController {
    private EpidService epidService;

    @GetMapping
    public ResponseEntity<Page<EpidReport>> getAll(Pageable pageable){
        return new ResponseEntity<>(epidService.getAll(pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EpidReport> getById(@PathVariable Long id){
        return new ResponseEntity<>(epidService.getById(id),HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<List<EpidReport>> getReportsByUserId(@PathVariable Long id){
        return new ResponseEntity<>(epidService.getReportsByUserId(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<EpidReport> create(@RequestBody CreateReportRequestDTO<Long, EpidCreateRequestDTO> createReportRequestDTO){
        return new ResponseEntity<>(epidService.create(createReportRequestDTO), HttpStatus.OK);
    }
}
