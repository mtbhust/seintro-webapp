package com.example.intro.se.controller;

import com.example.intro.se.dto.request.InitCreateRequestDTO;
import com.example.intro.se.dto.request.InitUpdateRequestDTO;
import com.example.intro.se.dto.response.InitResponseDTO;
import com.example.intro.se.service.InitService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/init")
@AllArgsConstructor
public class InitController {
    private final InitService initService;

    @GetMapping
    public ResponseEntity<Page<InitResponseDTO>> findAll(Pageable pageable){
        return new ResponseEntity<>(initService.findAll(pageable), HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<InitResponseDTO> create(@RequestBody InitCreateRequestDTO initCreateRequestDTO){
        return new ResponseEntity<>(initService.create(initCreateRequestDTO), HttpStatus.OK);
    }
    @PutMapping("/{id}")
    public ResponseEntity<InitResponseDTO> update(@PathVariable Long id, @RequestBody InitUpdateRequestDTO initUpdateRequestDTO){
        return new ResponseEntity<>(initService.update(id, initUpdateRequestDTO), HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public  ResponseEntity<InitResponseDTO> delete(@PathVariable Long id){
        return new ResponseEntity<>(initService.delete(id), HttpStatus.OK);
    }

}
