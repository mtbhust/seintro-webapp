package com.example.intro.se.controller;

import com.example.intro.se.dto.request.LoginRequestDTO;
import com.example.intro.se.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/login")
@AllArgsConstructor
public class AccountController {
    private AccountService accountService;

    @PostMapping
    public ResponseEntity<String> login(@RequestBody LoginRequestDTO loginRequestDTO){
        return new ResponseEntity<>(accountService.isLogin(loginRequestDTO.getUserName(),loginRequestDTO.getPassword()), HttpStatus.OK);
    }
}
