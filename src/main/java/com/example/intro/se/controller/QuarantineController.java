package com.example.intro.se.controller;

import com.example.intro.se.domain.CovidTest;
import com.example.intro.se.dto.request.Generic.CreateReportRequestDTO;
import com.example.intro.se.dto.request.QuarantineReportCreateRequestDTO;
import com.example.intro.se.dto.response.QuarantineResponseDTO;
import com.example.intro.se.service.QuarantineService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/quarantine")
@AllArgsConstructor
public class QuarantineController {
    private QuarantineService quarantineService;
    @GetMapping
    public ResponseEntity<Page<QuarantineResponseDTO>> getAll(Pageable pageable){
        return new ResponseEntity<>(quarantineService.findAll(pageable), HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<QuarantineResponseDTO> create(@RequestBody CreateReportRequestDTO<Long,QuarantineReportCreateRequestDTO> createRequestDTO){
        return new ResponseEntity<>(quarantineService.create(createRequestDTO), HttpStatus.OK);
    }

    @GetMapping("/{quarantineId}")
    public ResponseEntity<QuarantineResponseDTO> getQuarantineReportByQuarantineId(@PathVariable Long quarantineId){
        return new ResponseEntity<>(quarantineService.getQById(quarantineId), HttpStatus.OK);
    }

    @GetMapping("/tests/{quarantineId}")
    public List<CovidTest> getTestsByQuarantineId(@PathVariable Long quarantineId){
        ResponseEntity<List<CovidTest>> a =  new ResponseEntity<>(quarantineService.getListCovidTestsByQId(quarantineId), HttpStatus.OK);
        return a.getBody();
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<QuarantineResponseDTO>> getListQuarantineReportsById(@PathVariable Long userId){
        return new ResponseEntity<>(quarantineService.getListQuarantineByUserId(userId), HttpStatus.OK);
    }

}
