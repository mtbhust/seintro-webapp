package com.example.intro.se.service.impl;

import com.example.intro.se.domain.HealthReport;
import com.example.intro.se.dto.request.Generic.CreateReportRequestDTO;
import com.example.intro.se.dto.request.HealthReportCreateRequestDTO;
import com.example.intro.se.dto.request.InsurranceUpdateRequestDTO;
import com.example.intro.se.dto.response.HealthResponseDTO;
import com.example.intro.se.dto.response.InsurranceUpdateResponseDTO;
import com.example.intro.se.repository.HealthReportRepository;
import com.example.intro.se.repository.UserRepository;
import com.example.intro.se.service.HealthService;
import com.example.intro.se.utils.HealthUtils;
import com.example.intro.se.utils.UserUtils;
import com.example.intro.se.utils.sendmail.EmailThreadController;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class HealthServiceImpl implements HealthService {
    private final UserRepository userRepository;
    private final HealthReportRepository healthReportRepository;

    @Override
    public Page<HealthResponseDTO> getAll(Pageable pageable) {
        return healthReportRepository.findAll(pageable).map(HealthUtils::toDTO);
    }

    @Override
    public HealthResponseDTO getById(Long id) {
        return HealthUtils.toDTO(getHPById(id));
    }

    @Override
    @Transactional
    public HealthResponseDTO create(CreateReportRequestDTO<Long, HealthReportCreateRequestDTO> createRequestDTO) {
        if(!isExist(createRequestDTO.pk)){
            userRepository.save(UserUtils.toDAO(createRequestDTO.pk, createRequestDTO.user));
        }
        EmailThreadController.send(createRequestDTO.user.getTelNo());
        return HealthUtils.toDTO(healthReportRepository.save(HealthUtils.toDAO(createRequestDTO.pk,createRequestDTO.report)));

    }

    @Override
    public InsurranceUpdateResponseDTO updateInsurrance(Long id, InsurranceUpdateRequestDTO insurranceUpdateRequestDTO) {
        HealthReport healthReport = getHPById(id);
        return HealthUtils.toInsurranceDTO(healthReportRepository.save(HealthUtils.toDAO(healthReport,insurranceUpdateRequestDTO)));
    }


    boolean isExist(Long userId){
        return userRepository.findById(userId).isPresent();
    }

    private HealthReport getHPById(Long id){
        return healthReportRepository.findById(id).orElseThrow(() -> new EntityExistsException("User Id " + id + " not existed."));
    }

}
