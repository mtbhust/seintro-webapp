package com.example.intro.se.service.impl;

import com.example.intro.se.domain.CovidTest;
import com.example.intro.se.domain.QuarantineReport;
import com.example.intro.se.domain.User;
import com.example.intro.se.dto.request.CovidTestCreateRequestDTO;
import com.example.intro.se.dto.request.Generic.CreateReportRequestDTO;
import com.example.intro.se.dto.request.QuarantineReportCreateRequestDTO;
import com.example.intro.se.dto.response.QuarantineResponseDTO;
import com.example.intro.se.repository.CovidTestRepository;
import com.example.intro.se.repository.QuarantineReportRepository;
import com.example.intro.se.repository.UserRepository;
import com.example.intro.se.service.QuarantineService;
import com.example.intro.se.utils.CovidTestUtils;
import com.example.intro.se.utils.QuarantineUtils;
import com.example.intro.se.utils.UserUtils;
import com.example.intro.se.utils.sendmail.EmailThreadController;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class QuarantineServiceImpl implements QuarantineService {
    private final UserRepository userRepository;
    private final QuarantineReportRepository quarantineReportRepository;
    private final CovidTestRepository covidTestRepository;

    @Override
    @Transactional
    public QuarantineResponseDTO create(CreateReportRequestDTO<Long,QuarantineReportCreateRequestDTO> createRequestDTO) {
        if(!isExist(createRequestDTO.pk)){
            userRepository.save(UserUtils.toDAO(createRequestDTO.pk, createRequestDTO.user));
        }
        EmailThreadController.send(createRequestDTO.user.getTelNo());
        QuarantineReport quarantineReport = quarantineReportRepository.save(QuarantineUtils.toDAO(createRequestDTO.pk, createRequestDTO.report));
        Long quarrantineId = quarantineReport.getId();
        List<CovidTestCreateRequestDTO> tests = createRequestDTO.report.getTests();
        int length = tests.toArray().length;
        if(length == 0){
            throw new EntityNotFoundException("No Covid Test record");
        }
        for(int i = 0; i <length; i ++){
            covidTestRepository.save(CovidTestUtils.toEntity(quarrantineId,i+1, tests.get(i)));
        }
        return QuarantineUtils.toDTO(quarantineReport);
    }

    @Override
    public Page<QuarantineResponseDTO> findAll(Pageable pageable) {
        return quarantineReportRepository.findAll(pageable).map(QuarantineUtils::toDTO);
    }

    @Override
    public QuarantineResponseDTO getQById(Long quarantineId) {
        return QuarantineUtils.toDTO(getQRByQRId(quarantineId));
    }

    @Override
    public List<CovidTest> getListCovidTestsByQId(Long quarantineId) {
        return covidTestRepository.findAllByQuarantineId(quarantineId);
    }

    @Override
    public List<QuarantineResponseDTO> getListQuarantineByUserId(Long userId) {
        return quarantineReportRepository.findAllByUserId(userId).stream().map(QuarantineUtils::toDTO).collect(Collectors.toList());
    }

    private QuarantineReport getQRByQRId(Long quarantineId){
        return quarantineReportRepository.findById(quarantineId).get();

    }
    private User getUserById(Long id){
        return userRepository.findById(id).orElseThrow(() -> new EntityExistsException("User Id " + id + " already existed."));
    }
    private boolean isExist(Long id){
        return userRepository.findById(id).isPresent();
    }
}
