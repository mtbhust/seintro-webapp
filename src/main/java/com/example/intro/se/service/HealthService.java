package com.example.intro.se.service;

import com.example.intro.se.dto.request.Generic.CreateReportRequestDTO;
import com.example.intro.se.dto.request.HealthReportCreateRequestDTO;
import com.example.intro.se.dto.request.InsurranceUpdateRequestDTO;
import com.example.intro.se.dto.response.HealthResponseDTO;
import com.example.intro.se.dto.response.InsurranceUpdateResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HealthService {
    Page<HealthResponseDTO> getAll(Pageable pageable);
    HealthResponseDTO getById(Long id);
    HealthResponseDTO create(CreateReportRequestDTO<Long, HealthReportCreateRequestDTO> createRequestDTO);
    InsurranceUpdateResponseDTO updateInsurrance(Long id, InsurranceUpdateRequestDTO insurranceUpdateRequestDTO);

}
